<?php

/**
 * Scaffolder settings form.
 */
function scaffolder_admin_settings($form, &$form_state)
{
    $form['path'] = array(
        '#type' => 'fieldset',
        '#title' => t('Modules\' folder path'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    $form['path']['scaffolder_modules_path'] = array(
        '#type' => 'textfield',
        '#description' => t('Scaffolder will save modules in this path.'),
        '#default_value' => variable_get('scaffolder_modules_path'),
    );

    # This makes the system save all elements in their eponym variables.
    return system_settings_form($form);
}

/**
 * Module creation form.
 */
function scaffolder_module_creation_form($form, &$form_state)
{
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #  Settings
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    $form['info'] = array(
        '#type' => 'fieldset',
        '#title' => t('Module\'s settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    $form['info']['module_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Module\'s name'),
        '#description' => t('The display name of your module.'),
        '#required' => TRUE,
    );

    $form['info']['module_machine_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Module\'s machine name'),
        '#description' => t('The name used in the code.'),
        '#required' => TRUE,
    );

    $form['info']['module_description'] = array(
        '#type' => 'textfield',
        '#title' => t('Module\'s description'),
        '#description' =>
        t('The description is helpful for the module administration page.'),
        '#required' => TRUE,
    );

    $form['info']['module_package'] = array(
        '#type' => 'textfield',
        '#title' => t('Module\'s package'),
    );

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #  More settings
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    $form['info']['details'] = array(
        '#type' => 'fieldset',
        '#title' => t('More settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
    $form['info']['details']['help'] = array(
        '#type' => 'item',
        '#markup' => t('Lists are white space separated'),
    );

    $form['info']['details']['module_core'] = array(
        '#type' => 'textfield',
        '#title' => t('Core version'),
        '#description' =>
        t('Major Drupal version for this module. Default : core = 7.x'),
        '#default_value' => '7.x',
        '#required' => TRUE,
    );

    $form['info']['details']['stylesheets'] = array(
        '#type' => 'textfield',
        '#title' => t('Stylesheets'),
        '#description' =>
        t('CSS files to load on all pages. e.g. stylesheets[all][] = node.css'),
    );

    $form['info']['details']['scripts'] = array(
        '#type' => 'textfield',
        '#title' => t('Scripts'),
        '#description' =>
        t('Javascript files to load on all pages. e.g. scripts[] = somescript.js'),
    );

    $form['info']['details']['files'] = array(
        '#type' => 'textfield',
        '#title' => t('Files'),
        '#description' =>
        t('PHP files, classes, interfaces. e.g. files[] = example.test'),
    );

    $form['info']['details']['dependencies'] = array(
        '#type' => 'textfield',
        '#title' => t('Dependencies'),
        '#description' =>
        t('Required modules. e.g. dependencies[] = taxonomy'),
    );

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #  Files to create
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    $form['files'] = array(
        '#type' => 'fieldset',
        '#title' => t('Files to create'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    $form['files']['files_to_create'] = array(
        '#type' => 'checkboxes',
        '#options' => array(
            '.module' => '.module',
            '.inc' => '.inc',
            '.install' => '.install',
            '.pages' => '.pages.inc',
            '.forms' => '.forms.inc',
            '.api' => '.api.inc',
        ),
        '#default_value' => array(
            '.module',
            '.inc',
        ),
        '#required' => TRUE,
    );

    $form['files']['custom_files'] = array(
        '#type' => 'textfield',
        '#title' => t('Not found the wanted file suffix in the selection?'),
        '#description' =>
        t('If it\'s a common file suffix, don\'t hesitate to make a request to add it.')
    );


    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Create'),
    );


    return $form;
}

/**
 * Module creation form submit handler.
 */
function scaffolder_module_creation_form_submit($form, &$form_state)
{
    $module_name = $form_state['values']['module_name'];

    $module_machine_name = $form_state['values']['module_machine_name'];

    $module_path = variable_get('scaffolder_modules_path') . DIRECTORY_SEPARATOR .
            $module_machine_name;

    $files_to_create = array_filter($form_state['values']['files_to_create']);

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #  Create folder and files
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if (!mkdir($module_path))
    {
        drupal_set_message(
                t('The module folder @folder couldn\'t be created', array(
                    '@folder' => $module_path)), 'error');
        return;
    }

    # Changing rights for you
    chmod($module_path, 0775);

    # Store filenames for .info file.
    $filenames = array();

    foreach ($files_to_create as $extension)
    {
        $filename = $module_machine_name . $extension;

        $filepath = $module_path . DIRECTORY_SEPARATOR . $filename;

        $filenames[] = $filename;

        if (!file_put_contents($filepath, '<?php' . "\n"))
        {
            drupal_set_message(t('File @file couldn\'t be written. Abort.', array(
                        '@file' => $filepath)), 'error');
        }
        # Changing rights for you
        chmod($filepath, 0665);
    }

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #  Generate .info file
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    $info_file_path = $module_path . DIRECTORY_SEPARATOR . $module_machine_name .
            '.info';

    $info_output[] = 'name = ' . $module_name;
    $info_output[] .= 'description = ' . $form_state['values']['module_description'];
    $info_output[] .= 'core = ' . $form_state['values']['module_core'];

    foreach ($filenames as $filename)
    {
        $info_output[] .= 'files[] = ' . $filename;
    }

    // @todo add optional parameters process
    if (!file_put_contents($info_file_path, implode("\n", $info_output)))
    {
        drupal_set_message(t('File @file couldn\'t be written. Abort.', array(
                    '@file' => $info_file_path)), 'error');
        return;
    }
    # Changing rights for you
    chmod($info_file_path, 0665);

    drupal_set_message(t('Your module has been created.'));
}